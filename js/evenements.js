// Surveillance du focus du titre
surveillerFocus(titre_bd);
// Surveillance de la hauteur du titre pour décaler les objets si nécessaire
let lastHeight = titre_bd.clientHeight;
const observer = new MutationObserver(() => {
  const newHeight = titre_bd.clientHeight;
  let correctif = 0;
  if (newHeight !== lastHeight) {
    if (lastHeight===0){correctif=20;}
    else if (newHeight===0){correctif=-20;}
    ajuste(newHeight+correctif-lastHeight);
    lastHeight = newHeight;
  }
});
const config = { attributes: true, childList: true, subtree: true };
observer.observe(titre_bd, config);


// Écouteurs de souris
document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

input_themes.addEventListener('change', (e) => {
  type_galerie.choix=input_themes.value;
  change_variantes(input_themes.value);
});

input_variantes.addEventListener('change', (e) => {  
  affiche(licence,input_themes.value,input_variantes.value);
});

// Frappes clavier
document.addEventListener('keydown', function(event) {

    let capsLockOn = event.getModifierState('CapsLock');

    if (event.key === 'Delete') {
        supprime('clavier');
    }

    if ((event.ctrlKey && event.key === 'z') || (capsLockOn && event.key === 'Z')) {
      annuler();
    }

    if ((event.ctrlKey && event.key === 'y') || (capsLockOn && event.key === 'Y')) {
      refaire();
    }

    if (event.key === 'ArrowDown') {
      deplace(0,1);
    }

    if (event.key === 'ArrowUp') {
      deplace(0,-1);
    }

    if (event.key === 'ArrowLeft') {
      deplace(-1,0);
    }

    if (event.key === 'ArrowRight') {
      deplace(1,0);
    }


});
  

function surveillerFocus(editable) {
  editable.addEventListener('focus', function() {
    paragraphe_focus=editable;
    paragraphe_focus.focus=true;
  });
  editable.addEventListener('blur', function() {
    selectionEnCours = window.getSelection();
    PositionSelection = selectionEnCours.getRangeAt(0);
    positionCurseur = PositionSelection.startOffset;
    deselectionnerTexte();
  });
}
  

  document.addEventListener('dblclick', function() {
    ancre(event);
  });


// Désactiver le clic droit sauf pour les zones éditables
document.addEventListener('contextmenu', function (e) {
  // Vérifier si l'élément cliqué est éditable
  if (!e.target.isContentEditable) {
    e.preventDefault();
  }
});

// Emmpêcher le comportement par défaut du drag and drop
bd.addEventListener('dragover', (e) => {
  e.preventDefault();
});


// Déposer-glisser
bd.addEventListener('drop', (event) => {
  event.preventDefault();
  // Récupérez le fichier déposé
  const file = event.dataTransfer.files[0];

  // Vérifiez si le fichier est une image
  if (file && file.type.startsWith('image/')) {

    if (clicked){declique('auto')}

    const imageUrl = URL.createObjectURL(file);
    let objet=cree_image(event,'drop',imageUrl)
      //ancre(event,objet);
    
  }
});