
// Boutons crédits
bouton_credits.checked=true;
bouton_autres_credits.checked=false;

// Réglage des contrôles
police_taille.value = 24;
police_couleur.value = '#000000';
fond_couleur.value = '#ffffff';

bouton_annuler.disabled = true;
bouton_refaire.disabled = true;
bouton_ancrer.disabled = true;



// Tailles de polices
let tailles = [
    6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70];
// Ajout des tailles dans le menu déroulant
for (var i = 0; i < tailles.length; i++) {
    let option = document.createElement("option");
    option.text = tailles[i];
    option.classList.add('police');
    police_taille.add(option);
}

// Variables globales
redim=false;
dragged=null;
clicked=null;
paragraphe_focus=null;
hauteur=1;
bordureClic=5;
police_family='FuntypeRegular';
police_size=24;
vignette_background=null;
position=-1;
corry=0;
corrx=0;
borderWidth = 16;
lock=false;
ew_resize = 'col-resize';
ns_resize = 'row-resize';
let licence;
police_choix.value=police_family;
let reclic=false;
vignette_temporaire=null;
nb_fonds_perso=0;
decalage_bd=bd.offsetLeft+5;
decalage_bdy=bd.offsetTop+5;

// Listes
sauvegardes = [];
liste_credits = [];
sauvegardes_credits = [];
liste_credits_pour_paragraphe=[];
sauvegardes_liste_credits_pour_paragraphe=[];
liste_credits_polices_pour_paragraphe=[];
sauvegardes_liste_credits_polices_pour_paragraphe=[];

liste_sauvegarde_cache = [];

liste_bandes = [];
liste_vignettes = [];
liste_personnages = [];
liste_fonds = [];
liste_objets = [];

// Création de la Liste des personnages
for (let i = 1; i <= 20; i++) {
    let numero = i.toString().padStart(2, '0'); // Pour avoir le format '01', '02', etc.
    liste_personnages.push('perso' + numero + '.svg');
    liste_personnages.push('perso' + numero + '.png');
}
// Création de la liste des fonds
for (let i = 1; i <= 40; i++) {
  let numero = i.toString().padStart(2, '0'); // Pour avoir le format '01', '02', etc.
  liste_fonds.push(numero);
}
// Création de la Liste des objets
for (let i = 1; i <= 20; i++) {
  let numero = i.toString().padStart(2, '0'); // Pour avoir le format '01', '02', etc.
  liste_objets.push('objet' + numero + '.svg');
  liste_objets.push('objet' + numero + '.png');
}

// Détections
posXbd = bd.offsetLeft;
posYbd = bd.offsetTop;