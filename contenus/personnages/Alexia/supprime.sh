#!/bin/bash

# Parcourir tous les fichiers SVG récursivement
find . -type f -name "*.svg" | while read -r file; do
    # Supprimer toutes les occurrences de preserveAspectRatio="xMidYMin slice"
    sed -i '0,/<svg /s/preserveAspectRatio="xMidYMin slice"//' "$file"
    echo "Occurrences de preserveAspectRatio=\"xMidYMin slice\" supprimées dans $file"
done


