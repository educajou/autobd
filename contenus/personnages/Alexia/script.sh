#!/bin/bash

# Trouve tous les fichiers SVG dans le dossier courant et ses sous-dossiers
find . -type f -name "*.svg" | while read fichier_svg; do

    # Obtenez les valeurs de width et height du fichier SVG
    width=$(grep -oP 'width="\K[^"]+' "$fichier_svg")
    height=$(grep -oP 'height="\K[^"]+' "$fichier_svg")

    # Créez la valeur viewBox en utilisant les valeurs de width et height
    viewbox_value="0 0 $width $height"

    # Utilisez awk pour mettre à jour la ligne <svg> avec l'attribut viewBox
    awk -v width="$width" -v height="$height" -v viewbox="$viewbox_value" \
        'BEGIN {FS="\""} /<svg/ {gsub(/width="[0-9]+"/, "width=\"" width "\""); gsub(/height="[0-9]+"/, "height=\"" height "\""); print $1 "viewBox=\"" viewbox "\"" $2 } !/<svg/ {print}' \
        "$fichier_svg" > "$fichier_svg.tmp" && mv "$fichier_svg.tmp" "$fichier_svg"

    echo "L'attribut viewBox a été ajouté avec succès à $fichier_svg"

done

