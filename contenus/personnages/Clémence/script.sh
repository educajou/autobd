#!/bin/bash

# Trouve tous les fichiers SVG dans le dossier courant et ses sous-dossiers
find . -type f -name "*.svg" | while read fichier_svg; do

    # Obtenez les valeurs de width et height du fichier SVG
    width=$(grep -oP 'width="\K[^"]+' "$fichier_svg")
    height=$(grep -oP 'height="\K[^"]+' "$fichier_svg")

    # Créez la valeur viewBox en utilisant les valeurs de width et height
    viewbox_value="0 0 $width $height"

    # Utilisez sed pour ajouter l'attribut viewBox à la balise <svg>
    sed -i -e "/<svg/s/\(<svg[^>]*\)/\1 viewBox=\"$viewbox_value\"/" "$fichier_svg"

    echo "L'attribut viewBox a été ajouté avec succès à $fichier_svg"

done

